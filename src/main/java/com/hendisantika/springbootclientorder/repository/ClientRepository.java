package com.hendisantika.springbootclientorder.repository;

import com.hendisantika.springbootclientorder.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-client-order
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-23
 * Time: 17:40
 */
public interface ClientRepository extends JpaRepository<Client, Integer> {
}
