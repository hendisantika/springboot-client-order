package com.hendisantika.springbootclientorder.repository;

import com.hendisantika.springbootclientorder.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-client-order
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-23
 * Time: 17:42
 */
public interface OrderRepository extends JpaRepository<Order, Integer> {
    List<Order> findAll(Integer idClient);
}
