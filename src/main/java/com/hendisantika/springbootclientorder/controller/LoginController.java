package com.hendisantika.springbootclientorder.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-client-order
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-24
 * Time: 07:30
 */
@RestController
public class LoginController {

    @GetMapping("/resource")
    public Map<String, Object> home() {
        Map<String, Object> model = new HashMap<>();
        model.put("content", "Bienvenido");
        return model;
    }

    @GetMapping("/user")
    public Principal page(Principal user) {
        return user;
    }

}