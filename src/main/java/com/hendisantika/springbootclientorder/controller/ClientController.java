package com.hendisantika.springbootclientorder.controller;

import com.hendisantika.springbootclientorder.entity.Client;
import com.hendisantika.springbootclientorder.service.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Optional;


/**
 * Created by IntelliJ IDEA.
 * Project : springboot-client-order
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-23
 * Time: 18:12
 */
@RestController
@RequestMapping("/clients")
public class ClientController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;

    @GetMapping
    public ResponseEntity<List<Client>> clients() {
        LOGGER.info("Devolviendo lista de clients");
        List<Client> clients = clientService.getAll();
        if (clients.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok(clients);
    }

    @GetMapping("/{id}")
    public Optional<Client> getClient(@PathVariable("id") Integer id) {
        LOGGER.info("Recogiendo Client con id " + id);
        return clientService.findClient(id);
    }

    @PostMapping
    public ResponseEntity addClient(@RequestBody Client client,
                                    UriComponentsBuilder ucBuilder) {
        LOGGER.info("Guardando client " + client.getNombre());
        clientService.addClient(client);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/clients/{id}")
                .buildAndExpand(client.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteClient(@PathVariable("id") Integer id) {
        LOGGER.info("Recogiendo y Eliminando Client con id " + id);

        Optional<Client> client = clientService.findClient(id);
        if (client == null) {
            LOGGER.error("Error al eliminar client. Client con id "
                    + id + " no ha sido encontrado");
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("Error al eliminar client con id " + id);
        }
        clientService.deleteClient(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT)
                .body("Client " + id + "eliminado con exito");
    }

}