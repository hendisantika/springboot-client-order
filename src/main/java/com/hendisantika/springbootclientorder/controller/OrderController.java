package com.hendisantika.springbootclientorder.controller;

import com.hendisantika.springbootclientorder.entity.Order;
import com.hendisantika.springbootclientorder.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-client-order
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-24
 * Time: 07:31
 */
@RestController
@RequestMapping("/orders")
public class OrderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @GetMapping("/{idCliente}")
    public ResponseEntity<List<Order>> orders(
            @PathVariable("idClient") Integer idCliente) {
        List<Order> orders = orderService.getAllByClienteId(idCliente);
        if (orders.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        LOGGER.info("Devolviendo lista de orders");
        return ResponseEntity.ok(orders);
    }

    @PostMapping("/{idCliente}")
    public ResponseEntity addOrder(
            @PathVariable("idCliente") Integer idCliente,
            @RequestBody Order order,
            UriComponentsBuilder ucBuilder) {
        LOGGER.info("Guardando order "
                + order.getProducto() + " del cliente " + idCliente);
        orderService.addOrder(idCliente, order);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/orders/{idCliente}")
                .buildAndExpand(order.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @DeleteMapping("/{idCliente}/{idOrder}")
    public ResponseEntity deleteOrder(
            @PathVariable("idCliente") Integer idCliente,
            @PathVariable("idOrder") Integer idOrder) {
        LOGGER.info("Recogiendo y Eliminando Order "
                + idOrder + " del Cliente con id " + idCliente);

        Optional<Order> order = orderService.findOrder(idOrder);
        if (order == null) {
            LOGGER.error("Error al eliminar order. Order con id "
                    + idOrder + " no ha sido encontrado");
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("Error al eliminar order con id " + idOrder);
        }

        orderService.deleteOrder(idOrder);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Order "
                + idOrder + "eliminado con exito");
    }

}