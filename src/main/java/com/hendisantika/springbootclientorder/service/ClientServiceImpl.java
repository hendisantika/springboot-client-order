package com.hendisantika.springbootclientorder.service;

import com.hendisantika.springbootclientorder.entity.Client;
import com.hendisantika.springbootclientorder.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-client-order
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-23
 * Time: 17:48
 */
@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Client> getAll() {
        return clientRepository.findAll();
    }

    @Override
    @Transactional
    public void deleteClient(Integer id) {
        clientRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Optional<Client> findClient(Integer id) {
        return clientRepository.findById(id);
    }

    @Override
    @Transactional
    public void addClient(Client client) {
        clientRepository.save(client);
    }

}