package com.hendisantika.springbootclientorder.service;

import com.hendisantika.springbootclientorder.entity.Client;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-client-order
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-23
 * Time: 17:45
 */
public interface ClientService {
    List<Client> getAll();

    Optional<Client> findClient(Integer id);

    void addClient(Client client);

    void deleteClient(Integer id);

}
