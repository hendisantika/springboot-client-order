package com.hendisantika.springbootclientorder.service;

import com.hendisantika.springbootclientorder.entity.Order;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-client-order
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-23
 * Time: 17:52
 */
public interface OrderService {
    List<Order> getAllByClientId(Integer idClient);

    Optional<Order> findOrder(Integer idOrder);

    void addOrder(Integer i, Order Order);

    void deleteOrder(Integer idOrder);
}
