package com.hendisantika.springbootclientorder.service;

import com.hendisantika.springbootclientorder.entity.Client;
import com.hendisantika.springbootclientorder.entity.Order;
import com.hendisantika.springbootclientorder.repository.ClientRepository;
import com.hendisantika.springbootclientorder.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-client-order
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-23
 * Time: 17:55
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Order> getAllByClientId(Integer idClient) {
        return orderRepository.findAllById(Collections.singleton(idClient));
    }

    @Override
    @Transactional
    public void addOrder(Integer idClient, Order order) {
        Optional<Client> client = clientRepository.findById(idClient);
        order.setClient(client);
        client.addOrder(Order);

        clientRepository.save(client);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Order> findOrder(Integer idOrder) {
        return orderRepository.findById(idOrder);
    }

    @Override
    @Transactional
    public void deleteOrder(Integer idOrder) {
        orderRepository.deleteById(idOrder);
    }

}