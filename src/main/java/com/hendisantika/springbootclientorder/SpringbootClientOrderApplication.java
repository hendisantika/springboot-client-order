package com.hendisantika.springbootclientorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootClientOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootClientOrderApplication.class, args);
    }

}

